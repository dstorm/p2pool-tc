from distutils.core import setup, Extension

torrentcoin_module = Extension('torrentcoin_subsidy', sources = ['torrentcoin_subsidy.cpp'])

setup (name = 'torrentcoin_subsidy',
       version = '1.0',
       description = 'Subsidy function for TorrentCoin',
       ext_modules = [torrentcoin_module])
