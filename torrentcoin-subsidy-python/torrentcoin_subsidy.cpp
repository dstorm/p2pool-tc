#include <Python.h>

static const int64_t COIN = 100000000;
static const int64_t MAX_MONEY = 84000000 * COIN;

double ConvertBitsToDouble(unsigned int nBits){
    int nShift = (nBits >> 24) & 0xff;

    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    while (nShift < 29)
    {
        dDiff *= 256.0;
        nShift++;
    }
    while (nShift > 29)
    {
        dDiff /= 256.0;
        nShift--;
    }

    return dDiff;
}

int64_t static GetBlockBaseValue(int nBits,int nHeight)
{
    if(nHeight<1) return  MAX_MONEY * 0.005; //  0.5% Premine
    
    double dDiff = ConvertBitsToDouble(nBits);
    
    int64_t nSubsidy  = (2222222.0 / (pow((dDiff+2600.0)/9.0,2.0)));
    if (nSubsidy < 5) nSubsidy = 5;
    nSubsidy *= COIN;
    return nSubsidy;
}

static PyObject *torrentcoin_subsidy_getblockbasevalue(PyObject *self, PyObject *args)
{
    int input_bits;
    int input_height;
    if (!PyArg_ParseTuple(args, "ii", &input_bits, &input_height))
        return NULL;
    long long output = GetBlockBaseValue(input_bits, input_height);
    return Py_BuildValue("L", output);
}

static PyMethodDef torrentcoin_subsidy_methods[] = {
    { "GetBlockBaseValue", torrentcoin_subsidy_getblockbasevalue, METH_VARARGS, "Returns the block value" },
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC inittorrentcoin_subsidy(void) {
    (void) Py_InitModule("torrentcoin_subsidy", torrentcoin_subsidy_methods);
}
